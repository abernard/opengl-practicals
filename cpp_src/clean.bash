#!/bin/bash
root=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

cd $root/$1/sfmlGraphicsPipeline/extlib && make clean_all
cd $root/$1/sfmlGraphicsPipeline/ && rm -R ./build
cd $root/$1/sampleProject/ && rm -R ./build
