#!/bin/bash
root=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

cd $root/$1/sfmlGraphicsPipeline/extlib && make
cd $root/$1/sfmlGraphicsPipeline/ && mkdir build && cd ./build && cmake .. && make -j8
cd $root/$1/sampleProject/ && mkdir build && cd ./build && cmake .. && make
