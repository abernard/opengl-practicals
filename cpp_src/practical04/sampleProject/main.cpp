#include <ShaderProgram.hpp>
#include <Viewer.hpp>

#include <KeyframedCylinderRenderable.hpp>
#include <FrameRenderable.hpp>
#include <GeometricTransformation.hpp>
#include <iostream>
#include <iomanip>

void movingTree(Viewer& viewer);
void movingCylinder(Viewer& viewer);

void initialize_scene( Viewer& viewer )
{
    movingCylinder(viewer);
    movingTree(viewer);
}

int main() 
{
	Viewer viewer(1280,720);
	initialize_scene(viewer);

	while( viewer.isRunning() )
	{
		viewer.handleEvent();
		viewer.animate();
		viewer.draw();
		viewer.display();
	}	

	return EXIT_SUCCESS;
}

void movingCylinder( Viewer& viewer )
{
    //Add shader
    ShaderProgramPtr flatShader = std::make_shared<ShaderProgram>(  "../../sfmlGraphicsPipeline/shaders/flatVertex.glsl", 
                                                                    "../../sfmlGraphicsPipeline/shaders/flatFragment.glsl");
    viewer.addShaderProgram( flatShader );

    //Frame
    FrameRenderablePtr frame = std::make_shared<FrameRenderable>(flatShader);
    viewer.addRenderable(frame);

    viewer.getCamera().setViewMatrix( glm::lookAt( glm::vec3(0, -8, 4 ), glm::vec3(0, 0, 4), glm::vec3( 0, 0, 1 ) ) );

    //Animated cylinder
    auto cylinder = std::make_shared<KeyframedCylinderRenderable>(flatShader);
    cylinder->setParentTransform(glm::mat4(1.0));

    // TODO: Keyframes on parent transformation
    //cylinder->addParentTransformKeyframe(...);
    //...

    // TODO: Keyframes on local transformation
    //cylinder->addLocalTransformKeyframe(...);
    //...

    viewer.startAnimation();
    viewer.setAnimationLoop(true, 6.0);
}

void movingTree( Viewer& viewer )
{
    //Add shader
    ShaderProgramPtr flatShader = std::make_shared<ShaderProgram>(  "../../sfmlGraphicsPipeline/shaders/flatVertex.glsl", 
                                                                    "../../sfmlGraphicsPipeline/shaders/flatFragment.glsl");
    viewer.addShaderProgram( flatShader );

    //Frame
    FrameRenderablePtr frame = std::make_shared<FrameRenderable>(flatShader);
    viewer.addRenderable(frame);

    //Tree modeling:
    //The modeling is hierarchical (setLocalTransform, setParentTransform)
    //The animation is hierarchical too (addParentTransformKeyframe, addLocalTransformKeyframe)

    // TODO: Create and animate the main branch 
    KeyframedCylinderRenderablePtr root = std::make_shared<KeyframedCylinderRenderable>(flatShader);
    root->setLocalTransform( GeometricTransformation( glm::vec3{}, glm::quat(), glm::vec3{0.1,0.1,2.0}).toMatrix() );
    // TODO: root->addParentTransformKeyframe(...)
    // ...

    //TODO: Add and animate a child branch
    KeyframedCylinderRenderablePtr r1 = std::make_shared<KeyframedCylinderRenderable>(flatShader);
    //r1->addParentTransformKeyframe(...);
    //r1->addLocalParentTransformKeyframe(...);
    // ...
    //HierarchicalRenderable::addChild( root, r1 );
    // ...

    // TODO: Add and animate any other child branchs you want

    viewer.addRenderable( root );

    viewer.startAnimation();
    viewer.setAnimationLoop(true, 6.0);
}
