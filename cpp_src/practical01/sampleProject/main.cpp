#include <Viewer.hpp>

#define SCR_WIDTH 1024
#define SCR_HEIGHT 768

int main( int argc, char* argv[] )
{
	// Stage 1: Create the window and its OpenGL context
	Viewer viewer(SCR_WIDTH, SCR_HEIGHT);

	// Stage 2: Load resources like shaders, meshes... and make them part of the virtual scene
	// ...
	
	// Stage 3: Our program loop
	while( viewer.isRunning() )
	{
	    viewer.handleEvent(); 	// user interactivity (keyboard/mouse)
	    viewer.draw();		// rasterization (write in framebuffer)
	    viewer.display();		// refresh window
	}
	
	return EXIT_SUCCESS;
}
