#!/bin/bash
root=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

$root/clean.bash practical01
$root/clean.bash practical02
$root/clean.bash practical03
$root/clean.bash practical04
$root/clean.bash practical05
$root/clean.bash practical06
$root/clean.bash practical07

$root/clean.bash practical01_correction
$root/clean.bash practical02_correction
$root/clean.bash practical03_correction
$root/clean.bash practical04_correction
$root/clean.bash practical05_correction
$root/clean.bash practical06_correction
