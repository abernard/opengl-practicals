#!/bin/bash
root=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

$root/build.bash practical01
$root/build.bash practical02
$root/build.bash practical03
$root/build.bash practical04
$root/build.bash practical05
$root/build.bash practical06
$root/build.bash practical07

$root/build.bash practical01_correction
$root/build.bash practical02_correction
$root/build.bash practical03_correction
$root/build.bash practical04_correction
$root/build.bash practical05_correction
$root/build.bash practical06_correction

