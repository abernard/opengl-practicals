#!/bin/bash
root=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

cd $root/practical01/sampleProject/build/ && ./main
cd $root/practical02/sampleProject/build/ && ./main
cd $root/practical03/sampleProject/build/ && ./main
cd $root/practical04/sampleProject/build/ && ./main
cd $root/practical05/sampleProject/build/ && ./main
cd $root/practical06/sampleProject/build/ && ./main
cd $root/practical07/sampleProject/build/ && ./main

cd $root/practical01_correction/sampleProject/build && ./main
cd $root/practical02_correction/sampleProject/build && ./main
cd $root/practical03_correction/sampleProject/build && ./main
cd $root/practical04_correction/sampleProject/build && ./main
cd $root/practical05_correction/sampleProject/build && ./main
cd $root/practical06_correction/sampleProject/build && ./main
