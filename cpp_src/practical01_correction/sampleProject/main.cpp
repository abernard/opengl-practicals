#include <Viewer.hpp>
#include <ShaderProgram.hpp>
#include <FrameRenderable.hpp>
#include <CubeRenderable.hpp>
#include <IndexedCubeRenderable.hpp>

#include <glm/glm.hpp>

#define SCR_WIDTH 1024
#define SCR_HEIGHT 768

int main( int argc, char* argv[] )
{
  Viewer viewer(SCR_WIDTH, SCR_HEIGHT);

	std::string vShader = "./../../sfmlGraphicsPipeline/shaders/flatVertex.glsl";
	std::string fShader = "./../../sfmlGraphicsPipeline/shaders/flatFragment.glsl";
	ShaderProgramPtr defaultShader = std::make_shared<ShaderProgram>(vShader, fShader);
	viewer.addShaderProgram(defaultShader);

	FrameRenderablePtr frame = std::make_shared<FrameRenderable>(defaultShader);
	viewer.addRenderable(frame);

	CubeRenderablePtr cube1 = std::make_shared<CubeRenderable>(defaultShader);
	IndexedCubeRenderablePtr cube2 = std::make_shared<IndexedCubeRenderable>(defaultShader);
	viewer.addRenderable(cube1);
	viewer.addRenderable(cube2);

	glm::mat4 transform1; // identity matrix
	transform1 = glm::translate(transform1, glm::vec3(0.2, 0.5, 1.0) );
	transform1 = glm::rotate(transform1, 35.0f, glm::vec3(0.0, 1.0, 0.0) );
	transform1 = glm::scale(transform1, glm::vec3(0.7f) );
	cube1->setModelMatrix(transform1);

	glm::mat4 transform2; // identity matrix
	transform2 = glm::translate(transform2, glm::vec3(0.5, 1.0, 0.7) );
	transform2 = glm::rotate(transform2, 20.0f, glm::vec3(0.0, 0.0, 1.0) );
	transform2 = glm::scale(transform2, glm::vec3(0.5f) );
	cube2->setModelMatrix(transform2);

  while( viewer.isRunning() )
  {
      viewer.handleEvent();
      viewer.animate();
      viewer.draw();
      viewer.display();
  }

  return EXIT_SUCCESS;
}
