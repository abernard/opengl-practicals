\documentclass[a4paper,10pt]{article}
%\usepackage[utf8x]{inputenc}
\usepackage[latin1]{inputenc}
\usepackage[pdftex]{graphicx}
\usepackage{fullpage}
\usepackage{listings}
\usepackage{amsfonts}
\usepackage{tcolorbox}
\usepackage{color}
\newcommand{\vect}[1]{\mathbf{#1}}
\newcommand{\mat}[1]{#1}
\newcommand{\scalar}[1]{#1}
\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\set}[1]{\mathcal{#1}}

\newtheorem{exercice}{Question}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Prog en C
\lstset{language=C, basicstyle=\small \ttfamily,
         tabsize=2, breaklines=true, numberstyle=\tiny,
         %numbers=left,
         numbersep=11pt, stepnumber=1,
         xleftmargin=12mm,
         frame=leftline,
         framerule=4pt,
         rulecolor=\color{green}, 
         commentstyle=\color{blue},
         keywordstyle=\color{black}\bfseries,   
         showspaces=false,
         showstringspaces=false,   
	 moredelim=[is][\color{red}]{///}{///}}


%opening
\title{\LARGE \bf{Computer Graphics\\
Practical 3}}

\author{\includegraphics[width=6cm]{images/INSARennes}}

\date{INSA Fourth Year - 2018/2019\\
%Computer Graphics\\
Maud Marchal, Antonin Bernardin}
\begin{document}

\maketitle


\section{Objectives}
Hierarchical modeling is a way of composing complex objects out of multiple simpler primitives.
Objects are assembled in a hierarchical way. Their position are defined relatively to their parents using a {\bf combination} of transformations such as rotation, translation and scaling. A classical example of hierarchical model is a tree.\\
\includegraphics[width=4cm]{images/TP4_1.png}

\noindent
Transformations are combined using matrix products and homogeneous coordinates. For example: 
\begin{itemize}
\item a rotation R followed by a translation T will be described by the product TR;
\item a translation followed by a rotation will be described by the product RT.
\end{itemize}
{\bf Warning}: be careful about the order!

\begin{tabular}{cc}
\includegraphics[width=6cm]{images/RT_transformation.png} & \includegraphics[width=6cm]{images/TR_transformation.png} \\
Rotation then translation. & Translation then rotation.
\end{tabular}




%%%%%%%%%%%%%%%%%%%%%%%

\section{Additional files}
\begin{itemize}
\item Download the sources of the practical.
\item Copy the content of the archive in your project directory, *.hpp in \verb+/include+ and *.cpp in \verb+/src+.
\item Compile your project as in the previous practicals.
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%

\section{HierarchicalRenderable exercice}

\begin{itemize}
\item Update the renderables you implemented in the previous sessions so that they inherit from \verb+HierarchialRenderable+ in order to use hierarchical modeling: 
\begin{itemize}
\item Update the include:
\begin{lstlisting}{language=C}
#include "../include/Renderable.hpp"
\end{lstlisting}
becomes:
\begin{lstlisting}{language=C}
#include "../include/HierarchicalRenderable.hpp"
\end{lstlisting}
\item Update the inheritance:\\
In the header file:
\begin{lstlisting}{language=C}
class MyRenderable : public Renderable { /*...*/};
\end{lstlisting}
becomes:
\begin{lstlisting}{language=C}
class MyRenderable : public HierarchicalRenderable { /*...*/};
\end{lstlisting}
In the source file:
\begin{lstlisting}{language=C}
MyRenderable::MyRenderable(ShaderProgramPtr shaderProgram) :
    Renderable(shaderProgram) { ... } 
 \end{lstlisting}
 becomes:
 \begin{lstlisting}{language=C}
MyRenderable::MyRenderable(ShaderProgramPtr shaderProgram) :
    HierarchicalRenderable(shaderProgram) { ... } 
 \end{lstlisting}
\item Access to the model matrix using \verb+HierarchicalRenderable::getModelMatrix()+
\begin{lstlisting}{language=C}
glcheck(glUniformMatrix4fv(modelLocation, 1, GL_FALSE, glm::value_ptr(m_model)));
 \end{lstlisting}
becomes:
\begin{lstlisting}{language=C}
glcheck(glUniformMatrix4fv(modelLocation, 1, GL_FALSE, glm::value_ptr(getModelMatrix())));
 \end{lstlisting}
\end{itemize}
\item Read the \verb+HierarchicalRenderable+ class to understand how it works.
\item In the file \verb+HierarchicalRenderable.cpp+, there are several functions that must be filled to make \verb+HierarchicalRenderable+ work. We advice you to do it in the following order: 
\begin{itemize}
\item \verb+computeTotalParentTransform()+
\item \verb+updateModelMatrix()+
\end{itemize}
\item Build a hierarchical model in the main function using the following pattern:
\newpage
\begin{lstlisting}{language=C}
//...
#include "./../include/MyRenderable.hpp"
//...

void main()
{
    //...

    // create programs (you can use the same program for both the child and parent)
    ShaderProgramPtr parentProg = std::make_shared<ShaderProgram>( ... ) ;
    ShaderProgramPtr childProg = std::make_shared<ShaderProgram>( ... ) ;    
    viewer.addShaderProgram(parentProg);
    viewer.addShaderProgram(childProg);
           
    // Create renderables
    std::shared_ptr<MyRenderable> root = std::make_shared<MyRenderable>(parentProg);
    std::shared_ptr<MyRenderable> child1 = std::make_shared<MyRenderable>(childProg);
    
    // For each element of the hierarchy,
    // Set local transform and parent transform
    glm::mat4 rootParentTransform;
    root->setParentTransform(rootParentTransform);

    glm::mat4 child1ParentTransform;
    child1->setParentTransform(child1ParentTransform);    
    glm::mat4 child1LocalTransform;
    child1->setLocalTransform(child1LocalTransform);

    // Define parent/children relationships
    HierarchicalRenderable::addChild(root, child1);
        
    // Add the root of the hierarchy to the viewer
    viewer.addRenderable(root);

    //...
}

 \end{lstlisting}
\end{itemize}

\begin{tcolorbox}
{\bf  How to create transform with glm}\\
\noindent
You can find further details in the documentation of glm. Below, we provide a short, inefficient but safe and quick intro. 
\begin{lstlisting}{language=C}
//...
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
//...

/**Build a scale matrix
*
*Recommended usage: 
*glm::scale(glm::mat4(1.0), glm::vec3(xf,yf,zf))
*returns a scaling matrix that apply a xf scaling with respect to X axis,
*                                    a yf scaling with respect to Y axis 
*                                and a zf scaling with respect to Z axis.
*\param m Input matrix.
*\param scale Scale factor in each direction X,Y,Z.
*\return The scaling of m with respect to the vector scale. */
glm::mat4 glm::scale(const glm::mat4& m, const glm::vec3& scale);

/**Build a translation matrix
*
*Recommended usage: 
*glm::translate(glm::mat4(1.0), glm::vec3(xf,yf,zf))
*returns a translation matrix that apply the displacement vector (xf,yf,zf).
*\param m Input matrix.
*\param translation Displacement vector
*\return The translation of m with respect to the vector translation.*/
glm::mat4 glm::translate(const glm::mat4& m, const glm::vec3& translation);

/**Build a rotation matrix
*
*Recommended usage: 
*glm::rotate(glm::mat4(1.0), theta, glm::vec3(xf,yf,zf))
*returns a rotation matrix that apply a rotation of angle theta arround the axis (xf,yf,zf).
*\param m Input matrix.
*\param angle Angle of the rotation.
*\param axis Axis of the rotation.
*\return The rotation of m with respect to axis and with angle.*/
glm::mat4 glm::rotate(const glm::mat4& m, const float& angle, const& glm::vec3& axis);

 \end{lstlisting}
\end{tcolorbox}

\section{Project-related exercise}

In the final project you will have to build a dragoon or an unicor. Imagine the animal you would control and how to model its geometry from a combination of simple primitives and meshes. Start building the basic 3D model you will need to create the animal. 

\end{document}


