\documentclass[a4paper,11pt]{article}
%\usepackage[utf8x]{inputenc}
\usepackage[latin1]{inputenc}
\usepackage[pdftex]{graphicx}
\usepackage{fullpage}
\usepackage{listings}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{tcolorbox}
\usepackage{color}
\newcommand{\vect}[1]{\mathbf{#1}}
\newcommand{\mat}[1]{#1}
\newcommand{\scalar}[1]{#1}
\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\set}[1]{\mathcal{#1}}

\newtheorem{exercice}{Question}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Prog en C
\lstset{language=C, basicstyle=\small \ttfamily,
         tabsize=2, breaklines=true, numberstyle=\tiny,
         %numbers=left,
         numbersep=11pt, stepnumber=1,
         xleftmargin=12mm,
         frame=leftline,
         framerule=4pt,
         rulecolor=\color{green}, 
         commentstyle=\color{blue},
         keywordstyle=\color{black}\bfseries,   
         showspaces=false,
         showstringspaces=false,   
	 moredelim=[is][\color{red}]{///}{///}}


%opening
\title{\LARGE \bf{Computer Graphics\\
Practical 6: Local illumination}}

\author{\includegraphics[width=6cm]{img/INSARennes}}

\date{INSA Fourth Year - 2018/2019\\
%Computer Graphics\\
Maud Marchal, Antonin Bernardin}
\begin{document}

\maketitle

\paragraph{}
In all previous practicals, the scene was not really pleasant to see. Indeed, the depth of objects was not perceptible and the appearance was neither realistic nor expressive. It is high time to change that! In this practical we will implement basic local illumination techniques: nothing fancy, just enough to bring back the fun in your scene. 

\section{About the provided code}


\paragraph{}
The provided code enables to add different types of lights in the scene in order to lit objects and to reflect their material properties. Lights and material are based on the local illumination model of Phong.

\paragraph{}
For this practical, CPU and GPU codes are provided:
\begin{itemize}
\item CPU code is encapsulated into C++ classes that are gathered in a lighting subdirectory.
\item GPU code is gathered in the shaders subdirectory.
\end{itemize}

\paragraph{}
The first group of classes is the core of the lighting system:
\begin{itemize}
\item The \verb+DirectionalLight+, \verb+PointLight+, \verb+SpotLight+ classes which are all declared inside the file \verb+Light.hpp+. They define the attributes of the different kind of light in the Phong illumination model and provide functions to send these attributes to the GPU. 
\item The \verb+Material+ class. It defines the material of an object in the Phong illumination model.
\end{itemize}


\paragraph{}
The second group of classes corresponds to \verb+Renderable objects+ used for vizualisation:
\begin{itemize}
\item The \verb+DirectionalLightRenderable+, \verb+PointLightRenderable+, \verb+SpotLightRenderable+ classes. These classes allow to debug the parameters of your lights and animate your light or to control it with keyboard input. Indeed, such functionalities are easily implemented thanks to a \verb+Renderable+ class.
\item The \verb+LightedMeshRenderable+, \verb+LightedCylinderRenderable+ classes. They are similar to the renderable classes of previous practicals, but with the ability to be lit by lights. This is the kind of class you will need to adapt in your project to compute nice rendering for you scene.
\end{itemize}

\paragraph{}
The third group of code are the shaders. These are the files you will work on during this practical:
\begin{itemize}
\item The vertex shader \verb+phongVertex.glsl+.
\item The fragment shader \verb+phongFragment.glsl+.
\end{itemize}

\paragraph{}
{\bf The viewer class was also modified to take into lights.
We strongly advise you to update all the code and not only the aforementionned classes.}

\paragraph{}
Comment: you do not need to call \verb+make+ to update your shaders. Shaders are compiled at run-time. The Viewer can reload, compile and link them on demand if you press F3. Edit your shader sources and press F3 to see the results, without relaunching the executable.

\section{Light mecanism}

\paragraph{}
You already know that the fragment shader processes fragments. A fragment is a linear (trilinear in case of triangle primitives) interpolation of vertex attributes, projected onto the screen. If the fragment is on top of all primitive fragments (its depth is smaller), it will become a pixel.

\paragraph{}
Here, vertices have position and normal attributes. Thus, fragments will have those two attributes too and will represent surfels: surface elements. While it may look like fragments and surfels are the same, keep in mind that they are in different spaces: fragments are on the 2D screen (screen coordinates) while surfel are in the 3D world (world coordinates).

\paragraph{}
A light is applied to a surfel. According to the Phong model, this light will react to the position and normal of the surfel, and also to the material applied on the surface. We will explain here how the light react to such properties to compute the appearance of a surfel. You may find different models on the internet: that's fine, people are adapting the same model to suit their need.

\subsection{Light types}

\includegraphics[width=1\linewidth]{img/TP7_1.jpeg}  

Three kind of (standard) lights are used in this tutorial:
\begin{itemize}
\item A directional light represents a light which source is so far away from the camera that the light rays can be considered parallel when they reach an object, regardless of the position of the objects or the camera. A good example is the sun. For such a light, we have the following direction: $\overrightarrow{surfel\_to\_light} = -\overrightarrow{light\_direction}$
\item A point light is located at a given position, and illuminates in all directions. Good examples of point light are torches. Usually, the light intensity received by an objet decreases when its far from the source. In this case, we have this direction to light: $\overrightarrow{surfel\_to\_light}$=\verb+normalize+($light\_position - surfel\_position$).
\item A spot light is light which source is located somewhere in the environment, but rays are only shot in some directions instead of all around. An object receives light only if located in the cone defined by the spot. A good example of a spotlight would be a street lamp. The set of possible light ray directions is modeled by two cones with the same apex and directions. We will see their explanation in exercise 3 and exercise 4. 
\end{itemize}

\subsection{Phong's decomposition of light color}

\includegraphics[width=1\linewidth]{img/TP7_2.png}  

The Phong illumination model relies on the decomposition of a {\bf light L} color into three components: ambient, diffuse and specular. It is an empiric model which does not relies on the complex physical behavior of the light, but it has the advantage to be quite easy to understand and to implement.
\begin{itemize}
\item The ambient component $L_a$ specifies a minimum brightness. Even if there is no light ray directly hitting a surfel, the ambient component will lit a little this surfel, preventing it from being completely dark. The ambient component is constant for all surfels. It could be understood as what remains from a light's color after rays have infinitely bounced in every directions.
\item The diffuse component $L_d$ is the most important one to give a 3D appearance to a surfel. It models the light's color received by a ray directly hitting the surfel.
\item The specularity $L_s$ is the shiny component of the light, i.e. the color of a shiny spot on the object.
\end{itemize}

The color of a surfel is also divided into this three components. However, instead of being vertex attributes, those components are the same for the whole object. Combined, those components form the {\bf material} of such object. The material defines how a surfel will react to the ambient, diffuse and specular components of a light. Sometimes, the diffuse component of the material can be replaced by a vertex attribute: it depends on what you want to render. 

\subsection{Appearance computation}

\paragraph{}
The appearance of a surfel lit by a particular light $L$ is computed from:
\begin{itemize}
\item the color components of the light $L_a$, $L_d$, $L_s$,
\item the material properties of the object: $K_a$, $K_d$, $K_s$, and \verb+shininess+,
\item the position and the normal of the surfel \verb+surfel_position+, \verb+surfel_normal+,
\item and the other light properties (more about that in the exercises).
\end{itemize}
\paragraph{}
The appearance is computed component by component, then summed to obtain the contribution of a particular light to a surfel. When there are more than one light in the scene, the contributions of all lights are added. The result is the final appearance of a surfel, i.e. the color of its fragment.

\paragraph{}
Without taking into account distance attenuation and spot attenuation, the appearance computation for one light is given by:\\
\verb+vec3 ambient+ =  $1 \times L_a \times K_a$\\ 
\verb+vec3 diffuse+ = diffuse\_factor $\times L_d \times K_d$\\
\verb+vec3 specular+ = specular\_factor $\times L_s \times K_s$

\paragraph{}
The diffuse factor allows to decrease the diffuse component when the \verb+surfel_to_light+
and \verb+surfel_normal+ directions are too different. Indeed, when they are different, fewer photons will be reflected in the direction of the camera. The diffuse factor is then:\\
diffuse\_factor=max($\overrightarrow{surfel\_normal} \cdot \overrightarrow{surfel\_to\_light}$,0);

\paragraph{}
The maximum function is here to keep the diffuse factor non negative. It is valid since when the dot product is negative, the ray arrives from under the surface. Thus, the ray will never hit the surface on the direction we are interested in (assuming the normals of surfels are pointing in such direction). Using minimum or maximum to keep the values in valid ranges is a common thing in Computer Graphics and is referred to as clamping.

\paragraph{}
The specular factor is more difficult to understand. This is why we will only give its equation. It uses the reflected direction of the light relatively to the surfel normal:

\includegraphics[width=1\linewidth]{img/TP7_3.jpeg}  

specular\_factor=pow(max($\overrightarrow{surfel\_to\_camera} \cdot \overrightarrow{reflect\_direction}$,0),shininess)

\paragraph{}
Again, here we have a maximum function to avoid strange values (real power of a negative value) while remaining correct (if the dot product is negative, this means the camera is not in the cone of highly dense photons reflected by the surfel giving birth to a shiny spot).

\section{Exercise 1: Phong shading}

\begin{itemize}
\item Start the main program and check that you get the expected result:\\\\
\includegraphics[width=0.45\linewidth]{img/TP7_4.png} 

\item In \verb+phongFragment.glsl+, read carefully the \verb+computeDirectionalLight()+ function and make the parallel with the the equations of Phong.
\end{itemize}

\section{Exercise 2: Attenuation}

To get a more realistic approximation of the light, one cheap improvement is to simulate its intensity decay through a medium. In computer graphics, we model this attenuation $a$ thanks to three coefficients and the distance from the light to the surface of the lit object $d$:
\begin{equation}
a=\frac{1}{k_{constant}+k_{linear} \times d+k_{quadratic} \times d^2}
\end{equation}
The three factors $k_{constant}$, $k_{linear}$ and $k_{quadratic}$ are properties of a light, and are applied to the three illumination components computed of a surfel (ambient, diffuse and specular).
\begin{itemize}
\item In \verb+phongFragment.glsl+, in the function \verb+computePointLight()+, implement the aformentionned attenuation model and apply it to the light attributes.
\item Do the same for the spot light.
\item Check that you get a result similar to the one below.
\end{itemize}
\includegraphics[width=0.45\linewidth]{img/TP7_5.png} 

\pagebreak
\section{Exercise 3: Spot light}

A spot light cast light inside a cone of direction $\overrightarrow{spot\_direction}$ with an aperture $\theta$. Whenever a surfel is outside this cone, it is not lit by the spot (i.e. the spot intensity is set to 0 for this surfel). This case occurs when:\\
$ cos(\varphi) = \overrightarrow{surfel\_to\_light} \cdot (-\overrightarrow{spot\_direction}) < cos(\theta)$
\begin{itemize}
\item In \verb+phongFragment.glsl+, modify the function \verb+computeSpotLight()+ so that it takes into account the cone aperture and computes correctly the spot intensity factor.
\item Check that you get a result similar to the image below.
\end{itemize}
\includegraphics[width=0.45\linewidth]{img/TP7_6.png} 

\section{Exercise 4: Smooth spot edges}
You can see that the spot light has hard edges that are not likely to appear in the real world. To increase the realism of a spot light, we will use two cones of light. The first cone, named the inner cone, is the same as before, with an aperture of $\theta_{inner}$. The second cone is larger and referred to as the outer cone, with an aperture of $\theta_{outer}$. When a surfel is between the inner and the outer cone, its intensity value will be between 0 and 1. If the surfel is inside the inner cone, its intensity is still 1, while when outside both cones, its intensity is set to 0. To compute the spot intensity, we have the following nice formula: 
\begin{equation}
intensity = clamp ( \frac{cos(\varphi) - cos(\theta_{outer})}{cos(\theta_{inner}) - cos(\theta_{outer})}, 0, 1)
\end{equation}
\begin{itemize}
\item Implement this other version of the spot light intensity.
\item Check that you get a result similar to the expected one.
\end{itemize}
\includegraphics[width=0.45\linewidth]{img/TP7_7.png} 
\end{document}
