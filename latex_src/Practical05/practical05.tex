\documentclass[a4paper,11pt]{article}
%\usepackage[utf8x]{inputenc}
\usepackage[latin1]{inputenc}
\usepackage[pdftex]{graphicx}
\usepackage{fullpage}
\usepackage{listings}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{tcolorbox}
\usepackage{color}
\newcommand{\vect}[1]{\mathbf{#1}}
\newcommand{\mat}[1]{#1}
\newcommand{\scalar}[1]{#1}
\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\set}[1]{\mathcal{#1}}

\newtheorem{exercice}{Question}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Prog en C
\lstset{language=C, basicstyle=\small \ttfamily,
         tabsize=2, breaklines=true, numberstyle=\tiny,
         %numbers=left,
         numbersep=11pt, stepnumber=1,
         xleftmargin=12mm,
         frame=leftline,
         framerule=4pt,
         rulecolor=\color{green}, 
         commentstyle=\color{blue},
         keywordstyle=\color{black}\bfseries,   
         showspaces=false,
         showstringspaces=false,   
	 moredelim=[is][\color{red}]{///}{///}}


%opening
\title{\LARGE \bf{Computer Graphics\\
Practical 5: Physically-based animation}}

\author{\includegraphics[width=6cm]{images/INSARennes}}

\date{INSA Fourth Year - 2018/2019\\
%Computer Graphics\\
Maud Marchal, Antonin Bernardin}
\begin{document}

\maketitle

\paragraph{}
The aim of this practical is to animate a scene using a simple physics-based system. It relies on particles only, which are updated via the application of forces (gravity, springs, etc.). These particles can be rendered directly, or used as control positions by more complex objects.

\section{Particle system}

\paragraph{}
A particle is defined at time $t$ by its position $\vec{x}(t)$ and its velocity $\vec{v}(t)$, as well as its mass $m$.

\subsection{Law of motion and system update}
\paragraph{}
Following \textbf{Newton's second law}, each particle is accelerated via all the \textit{forces} exerted on it, with the relation:

\[
m \frac{d\vec{v}(t)}{dt} = \sum \vec{f(t)}
\]

In a discrete system, the state of a particle at time $t+dt$ can then be computed out of its previous state and all the forces applied at time $t$. This yields to a differential system, which must be solved using an \textbf{integration scheme}. In this practical, a simple \textit{explicit Euler} scheme is used to successively compute the new velocity then the new position of each particle:

\[
\left\lbrace
\begin{array}{l c l}
\vec{v}(t+dt) & = & \vec{v}(t) + \frac{1}{m} dt \sum \vec{f}\\
\vec{x}(t+dt) & = & \vec{x}(t) + dt \vec{v}(t+dt) \\
\end{array}
\right.
\]

\subsection{Models of forces}
\paragraph{}
Various forces will be applied in our system, especially gravity and global damping.
Two particles could also be linked by a spring, which generates opposite forces on each of its extremities.

\newpage
\paragraph{Gravity}
\paragraph{}
On Earth, gravity $g$ is uniform and exerts a force, called weight, proportional to the mass. For each particle $i$, the force is following: 

\[
\vec{f}_{G \rightarrow i} = m_i \vec{g}
\]

\paragraph{Damped spring}
\paragraph{}
Springs are used to simulate an elastic behavior that tends to bring two particles back to a given distance from each other, the \textit{equilibrium length}. An ideal spring between particles $i$ and $j$ is parametrized by its equilibrium length $l_0$ and its stiffness $k$. The force exerted on particle $i$ is then:

\[
\vec{f^k}_{j \rightarrow i} = -k \left(\left\Vert \vec{x_i}-\vec{x_j} \right\Vert -l_0 \right) 
\frac{\vec{x_i}-\vec{x_j}}
{\left\Vert \vec{x_i}-\vec{x_j} \right\Vert}
\]

Ideal springs quickly yield to unstable simulation. To prevent this, \textit{damping} is used to reduce the relative motion between two particles. The action of a damper is proportional to the viscous damping coefficient $k_c$ and the relative velocity
(difference of velocities of the two particles projected on the spring direction):

\[
\vec{f^{k_c}}_{j \rightarrow i} = 
-k_c\left(\left(\vec{v_i} - \vec{v_j}\right)
\cdot \frac{\vec{x_i}-\vec{x_j}} {\left\Vert \vec{x_i}-\vec{x_j} \right\Vert}
\right)
\frac{\vec{x_i}-\vec{x_j}} {\left\Vert \vec{x_i}-\vec{x_j} \right\Vert}
\]

The total action of a damped spring is computed by summing the different contributions of an ideal spring and a damper:

\[
\vec{f}_{j\rightarrow i} = \vec{f^k}_{j\rightarrow i} + \vec{f^{k_c}}_{j \rightarrow i}
\]

From \textbf{Newton's third law}, the force generated at the other extremity of the spring is simply the opposite :

\[
\vec{f}_{i\rightarrow j} = - \vec{f}_{j\rightarrow i} 
\]

\subsection{Global damping: viscosity of the medium}

\paragraph{}
The movements of all bodies moving within a medium are damped according to its global viscosity. This is mostly relevant in fluids or windy environments.
This phenomenon can be modeled by an action proportional to the medium viscosity coefficient $c$ exerted on the opposite direction of the velocity:
\[
\vec{f}_{C\rightarrow i} = -c \vec{v_i}(t)
\]

In animation, global damping can also be used to simply slow the system down thus increasing its numerical stability.

\subsection{Collisions}

\paragraph{}
Two kind of collisions are handled in our dynamic system: between a particle and a (fixed) plane, and between two particles.
In all cases, two steps are required:

\begin{itemize}

\item 
\textbf{Collision detection}. Usually, a broad phase first detects (in an efficient way) if two objects \textit{could} intersect. In that case, a narrow phase actually computes whether the two objects are really inter-penetrating or not.
Since we only deal with few and very simple objects, the broad phase will not be implemented in this practical.

\item 
\textbf{Response to collision}. In our case, the position and velocity of each particle will be modified to correct the penetration.
Part of the energy could be absorbed during the impact, which would slow down the particles.

\end{itemize}


\paragraph{}
Let's consider two particles $p_{1}$ and $p_{2}$ with a position $ \left\lbrace \vec{x_{i}} \right\rbrace_{i=1,2} $, a velocity pre-collision $ \left\lbrace \vec{u_{i}} \right\rbrace_{i=1,2}$, a velocity post-collision $ \left\lbrace \vec{v_{i}} \right\rbrace_{i=1,2}$, a mass $ \left\lbrace m_{i} \right\rbrace_{i=1,2}$ and a radius $ \left\lbrace r_{i} \right\rbrace_{i=1,2} $.

\paragraph{Collision between two particles}
\begin{itemize}
\item 
\textbf{Detection}: two particles intersect each other if the distance between their center is less than the sum of their radius:
\[
\left\Vert \vec{x_1} - \vec{x_2} \right\Vert \leq r_1 + r_2
\]
In that case, let's set $\vec{k}$ the vector between both centers 
\[  \vec{k} = \frac{\vec{x_1} - \vec{x_2}} {\left\Vert \vec{x_1} - \vec{x_2} \right\Vert} \]

and $\mathit{pen}$ the interpenetration distance along $\vec{k}$ 
\[  \mathit{pen} = r_1 + r_2 - \left\Vert \vec{x_1} - \vec{x_2} \right\Vert \] 

\includegraphics[width=0.3\linewidth]{images/collision_particle_particle}             

\item 
\textbf{Correction of the positions}: each particle is \textit{moved} along $\vec{k}$, to be in contact but without interpenetration.
Both particles move half of the interpenetration distance, with a ponderation if their masses differ (the heavier moves less). If one of the particle is fixed, only the other is displaced.

\[
\left\lbrace
\begin{array}{ccc}
\vec{x_{1}} & \leftarrow & \vec{x_{1}} + \left(\frac{m_2}{m_1+m_2} \mathit{pen}\right) \vec{k} \\
\vec{x_{2}} & \leftarrow & \vec{x_{2}} - \left(\frac{m_1}{m_1+m_2} \mathit{pen}\right) \vec{k} \\
\end{array}
\right.
\]

\item 
\textbf{Correction of the velocities}:

\[
\left\lbrace
\begin{array}{ccc}
\vec{v_{1}} & = & \vec{u_{1}} - \frac{a}{m_{1}}\vec{k} \\
\vec{v_{2}} & = & \vec{u_{2}} + \frac{a}{m_{2}}\vec{k} \\
\end{array}
\right.
\]

with:
\[
a = \frac{(1+e)\vec{k} \cdot \left( \vec{u_{1}} - \vec{u_{2}} \right)}{ \left( \frac{1}{m_{1}} + \frac{1}{m_{2}} \right)}
\]

$e$ is the \textit{restitution coefficient}, which ranges from 0.0 (full absorption of the energy by the impact) to 1.0 (full elastic response).

\paragraph{}
The new velocities of the particles are computed so that the linear momentum and kinetic energy are conserved (in the elastic case).
\begin{itemize}
\item 
Conservation of linear momentum:

\[
m_{1}\vec{u_{1}} + m_{2}\vec{u_{2}} = m_{1}\vec{v_{1}} + m_{2}\vec{v_{2}}
\]

\item 
Conservation of kinetic energy:
\[
\frac{1}{2}m_{1} \vec{u_{1}} \cdot \vec{u_{1}} + \frac{1}{2}m_{2} \vec{u_{2}} \cdot \vec{u_{2}} = \frac{1}{2}m_{1} \vec{v_{1}} \cdot \vec{v_{1}} + \frac{1}{2}m_{2} \vec{v_{2}} \cdot \vec{v_{2}}
\]

\end{itemize}
The resolution of this system (2 equations, 2 unknowns) leads to the above result.

\end{itemize}


\paragraph{Collision between a particle and a plane}
\paragraph{}
A plane $\pi$ (supposed infinite and fixed) is defined by the equation:

\[ 
\begin{array}{lrcc}
ax + by + cz + d = 0 & \Leftrightarrow & \vec{n} \cdot \vec{p} = d & (\pi) \\
\end{array}
\]

where $\vec{n}=(a,b,c)$ is the normal of the plane (normalized), $\vec{p}=(x,y,z)$ is a point of the plane and $d$ is the distance from the plane to the origin.
\paragraph{}
The distance between a point $\vec{q}$ and the plane is given by $\left|\left(\vec{q} - \vec{p} \right) \cdot \vec{n}\right|$\\

\includegraphics[width=0.2\linewidth]{images/plane.png}   

\begin{itemize}
\item \textbf{Detection}: 
there is penetration if the distance between the particle center and the plane is less than the radius of the particle: $\left|\left(\vec{x_1} - \vec{p} \right) \cdot \vec{n}\right| \leq r_1$ . Let's call this distance $\mathit{d2Plane}.$\\

\includegraphics[width=0.5\linewidth]{images/collision_particle_plane.png}  

\item 
\textbf{Correction of the position}: the particle is projected as if the plane fully absorbed the impact, along the plane normal:
\[
\begin{array}{ccc}
\vec{x_1} & \leftarrow & \vec{x_1} - \left(\mathit{d2plane} -  r_1 \right) \vec{n} 
\end{array}
\]

\item 
\textbf{Correction of the velocity}: 
\[
\begin{array}{ccc}
\vec{v_1} & = & \vec{u_1} - \left(1 + e\right) \left(\vec{u_1} . \vec{n}\right) \vec{n} 
\end{array}
\]

\paragraph{} 
The plane case is actually similar to the particle/particle collision. Just consider that the plane is particle 2, with a null velocity (fixed) and an infinite mass. Vector $\vec{k}$ is colinear with the plane normal.

\end{itemize}

\section{About the provided code}
\paragraph{}
The provided code enables to model a system composed of particles linked together with damped springs, within a medium with gravity and viscosity. Particles are represented by small spheres, with a radius and a mass.
Collisions are handled to ensure non-penetration between particles and infinite planes, and between pairs of particles.


\paragraph{}
All classes are gathered in a \texttt{dynamics} subdirectory. The first group of class is the core of the dynamics system:

\begin{itemize}
\item 
The main class is \texttt{DynamicSystem}, which gathers all components and run the system resolution over time;

\item 
\texttt{Particle} represent a single particle, with its own properties (position, velocity, mass, radius) and the forces applied to it;

\item 
Forces are modeled in the \texttt{ForceField} hierarchy, specialized with classes \texttt{ConstantForceField} (e.g. for gravity), \texttt{SpringForceField} or \texttt{DampingForceField} (e.g. for the viscosity of the medium);

\item 
Collisions between objets are detected and solved in the \texttt{Collision} hierarchy, specialized by the \\\texttt{ParticlePlaneCollision} and \texttt{ParticleParticleCollision} classes;

\item 
Finally the integration scheme is delegated to a solver, with a single implementation in \texttt{EulerExplicitSolver}.

\item 
This physics code is incomplete, and you will need to write part of it.

\end{itemize}

\paragraph{} 
\includegraphics[width=1.0\linewidth]{images/uml_dynamics.png}         
\paragraph{}
The second group of class are \texttt{Renderable} objects used for vizualisation:

\begin{itemize}
\item 
\texttt{DynamicSystemRenderable} handles the animation loop and control keys, but do not draw anything. Dynamics renderables should be added as children of the DynamicSystemRenderable;

\item 
All particles can be rendered using a single \texttt{ParticleListRenderable} object;

\item 
Each plane is rendered using a \texttt{QuadRenderable} instance;
\item 
Finally, each force field (springs, constants, damping) can be displayed using its associated renderable class.

\end{itemize}

\section{Exercise 1: simple particles}
\paragraph{}
All scenes of these exercises are built in \texttt{main.cpp}. Let's start with the \texttt{initialize\_scene} function.
\begin{itemize}            
\item 
Study how the dynamics system is created, containing two particles and the gravity force field.

\item 
To understand how the dynamic system works, start with the function \texttt{computeSimulationStep()}. This is the heart of the system. Now compile and run the animation. Deduce from the function computeSimulationStep() why the particles stay put.

\item 
You must first complete the integration scheme in \texttt{EulerExplicitSolver::do\_solve(...)} to update the velocity and position of each particle. Now, the particles should fall down as expected.

\paragraph{} 
Several keys are available to control the dynamic system animation (the code is in\\\texttt{DynamicSystemRenderable::do\_keyPressedEvent(sf::Event \&e)}):
\begin{itemize}
\item \texttt{F4}: play/pause the animation
\item \texttt{F5}: reset the animation
\item \texttt{t}: "tilt" all particles, a brutal way to randomly animate the system... 
\end{itemize}
More generally, press the \texttt{F1} key to display all the available options.

\end{itemize}

\section{Exercise 2: it's spring time}

\paragraph{}
In the \texttt{initialize\_scene} function, load the scene defined in \texttt{springs} function.

\begin{itemize}

\item 
Look at the created system, a 2D net of particles linked with springs. Border particles are fixed.

\item 
Complete the \texttt{SpringForceField::do\_addForce()} method to implement a damped spring. Forces must be computed then added to the particles at each extremity.

\item  
Run the simulation: the system should dangle... Does it stop? How can you explain this?

\item  
In this practical, a  global damping is added to dissipate the velocity of all particles in a medium viscosity (like air friction). Tune the \texttt{dampingCoefficient} value of the global damping and the \texttt{damping} value of the springs to get a realistic simulation (a balance must be found between stable and too slow...)  

\end{itemize}

\section{Exercise 3: collisions}

\paragraph{}
Load the scene \texttt{collisions}, containing several particles and a plane. They will collide each other... once you will have done with this exercise.

\begin{itemize}

\item 
Run the animation a first time: nothing is treated...

\item 
Do it again after activating the collision detection in the scene creation using\\\texttt{DynamicSystem::setCollisionDetection(true)}.\\So far, only collisions between particles are implemented. Study the code in\\\texttt{ParticleParticleCollision::do\_solveCollision()}, corresponding to the collisions theory.

\item 
Vary the restitution coefficient of the dynamics system between 0.0 and 1.0 to observe its effect.\\Set it to 1.0 and let the simulation run at least 30 seconds: what happens? How could you explain it? 

\item       
Based on the above theory and the previous particle/particle case, complete the \texttt{ParticlePlaneCollision} class to detect the collision and solve the contact.

\item 
Initialize the horizontal velocity of the particles with a non null value. Run the animation and check if the system behaves as expected (be logical).

\end{itemize}

\paragraph{} 
Collisions are resolved by pair, between particles and planes then among particles. When many collisions occur, is there any guarantee that they will not be any inter-penetration at the end of the iteration? No! Any idea why? (\textit{nb: this is a very difficult problem to resolve!})\\This may be observed by adding one or more planes to the scene, and increasing the number of particles (in our case however, the double loop is quite efficient...).

\section{Exercise 4: let's play pool}

\begin{itemize}

\item 
In this last exercise, load the scene \texttt{playPool} by calling the corresponding function in \texttt{main.cpp}: two particles are set on an horizontal plane, and bounded by four vertical planes. The first particle can be moved (accelerated) interactively with the arrows keys. Lets' play!

\item 
Carefully study the scene creation as well as the \texttt{ControlledForceFieldRenderable} class.          

\item 
You now have many animation examples using a dynamic system. You can now implement your own physics-based animations to enhance your dragon or unicorn, by adding fireballs for instance, or any other amazing elements.

\end{itemize}

\end{document}
